const token = function(req, res, next) {
  const access_token = req.cookies.token || req.headers["Authorization"];
  if (!access_token) {
  }
  next();
};

export default token;
