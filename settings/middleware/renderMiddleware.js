import fs from "fs";
import path from "path";
import serialize from "serialize-javascript";

import React from "react";
import { Helmet } from "react-helmet";
import { Provider } from "react-redux";
import { StaticRouter } from "react-router-dom";
import { renderToString } from "react-dom/server";

import PATH from "../path";
import store from "../../src/helpers/createStore";
import Root from "../../src/Root";

const render = (req, res) => {
  const routeContext = {};
  const file = path.resolve(PATH.build, "index.html");
  fs.readFile(file, "utf8", (err, html) => {
    if (err) {
      console.error(err);
      return res.status(404).end();
    }

    const container = renderToString(
      <Provider store={store}>
        <StaticRouter location={req.originalUrl} context={routeContext}>
          <Root />
        </StaticRouter>
      </Provider>
    );

    const helmet = Helmet.renderStatic();
    const initialData = serialize(store.getState());

    html = html.replace("<head>", "<head>" + helmet.title.toString());
    html = html.replace("<head>", "<head>" + helmet.meta.toString());
    html = html.replace("<head>", "<head>" + helmet.link.toString());

    html = html.replace(
      '<div id="root"></div>',
      `<div id="root"></div><script type="text/javascript" charset="utf-8">window.__REDUX_STATE__ = ${initialData};</script>`
    );
    html = html.replace(
      '<div id="root"></div>',
      `<div id="root">${container}</div>`
    );

    return res.send(html);
  });
};

export default render;
