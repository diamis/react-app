const path = require("path");

const prefix = "../";
const PATHS = {
  build: path.resolve(__dirname, prefix, "build"),
  static: path.resolve(__dirname, prefix, "build/static"),
  public: path.resolve(__dirname, prefix, "public"),
  src: path.resolve(__dirname, prefix, "src"),

  entryClient: path.resolve(__dirname, prefix, "src/entry-client.js"),
  entryServer: path.resolve(__dirname, prefix, "src/entry-server.js")
};

module.exports = PATHS;
