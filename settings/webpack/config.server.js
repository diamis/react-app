const dotenv = require("dotenv");

const PATHS = require("../path");
const alias = require("./resolve_alias");

dotenv.config();

const ENV = process.env;
const PORT = process.env.PORT || 3000;
let isEnvProduction = ENV.NODE_ENV === "production";
let isEnvDevelopment = ENV.NODE_ENV === "development";
if (!isEnvProduction && !isEnvDevelopment) isEnvDevelopment = true;

module.exports = {
  name: "server",
  target: "node",
  entry: ["@babel/polyfill", PATHS.entryServer],
  output: {
    path: PATHS.build,
    publicPath: isEnvDevelopment ? `http://localhost:${PORT}/` : "/",
    filename: "server.js",
    libraryTarget: "commonjs2"
  },
  resolve: {
    extensions: [".js", ".jsx"],
    alias: alias
  },
  module: {
    rules: [
      {
        test: /\.(js|mjs|jsx|ts|tsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env", "@babel/preset-react"],
            plugins: [
              "@babel/plugin-proposal-object-rest-spread",
              "@babel/plugin-proposal-class-properties",
              "@babel/plugin-syntax-dynamic-import",
              "react-loadable/babel"
            ]
          }
        }
      },
      {
        test: /\.(bmp|png|jpg|jpeg|gif|svg)$/,
        use: {
          loader: "file-loader",
          options: {
            name: "static/media/[name].[hash:4].[ext]",
            emitFile: false,
            emit: false
          }
        }
      },
      {
        test: /\.(eot|ttf|woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
        use: {
          loader: "file-loader",
          options: {
            name: "static/fonts/[name].[ext]",
            emitFile: false
          }
        }
      },
      {
        test: /\.(css|less|scss|sass)$/,
        use: "ignore-loader"
      }
    ]
  },
  node: {
    __dirname: false
  },
  stats: {
    colors: true
  }
};
