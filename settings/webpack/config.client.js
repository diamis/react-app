const path = require("path");
const dotenv = require("dotenv");
const webpack = require("webpack");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const ManifestPlugin = require("webpack-manifest-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const PATHS = require("../path");
const alias = require("./resolve_alias");

dotenv.config();
const ENV = process.env;

const PORT = process.env.PORT || 3000;
let isEnvProduction = ENV.NODE_ENV === "production";
let isEnvDevelopment = ENV.NODE_ENV === "development";
if (!isEnvProduction && !isEnvDevelopment) isEnvDevelopment = true;

// style files regexes
const cssRegex = /\.css$/;
const cssModuleRegex = /\.module\.css$/;
const sassRegex = /\.(scss|sass)$/;
const sassModuleRegex = /\.module\.(scss|sass)$/;

function getStyleLoaders(cssOptions, preProcessor) {
  const Loaders = [
    { loader: MiniCssExtractPlugin.loader, options: { hmr: isEnvDevelopment } },
    { loader: "css-loader", options: cssOptions }
  ];

  if (preProcessor) Loaders.push({ loader: preProcessor });
  return Loaders.filter(Boolean);
}

module.exports = {
  name: "client",
  target: "web",
  mode: isEnvProduction ? "production" : "development",
  entry: [
    isEnvDevelopment &&
      `webpack-hot-middleware/client?path=http://localhost:${PORT}/__webpack_hmr`,
    "@babel/polyfill",
    PATHS.entryClient
  ].filter(Boolean),
  output: {
    path: PATHS.build,
    pathinfo: isEnvDevelopment,
    publicPath: isEnvDevelopment ? `http://localhost:${PORT}/` : "/",
    filename: isEnvProduction
      ? "static/js/[name].[contenthash:4].js"
      : isEnvDevelopment && "static/js/bundle.js",
    chunkFilename: isEnvProduction
      ? "static/js/[name].[contenthash:4].chunk.js"
      : isEnvDevelopment && "static/js/[name].chunk.js",
    hotUpdateMainFilename: "updates/[hash].hot-update.json",
    hotUpdateChunkFilename: "updates/[id].[hash].hot-update.js"
  },
  resolve: {
    extensions: [".js", ".jsx"],
    alias: alias
  },
  module: {
    rules: [
      {
        test: /\.(js|mjs|jsx|ts|tsx)$/,
        include: PATHS.src,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env", "@babel/preset-react"],
            plugins: [
              "@babel/plugin-proposal-object-rest-spread",
              "@babel/plugin-proposal-class-properties",
              "@babel/plugin-syntax-dynamic-import",
              "react-loadable/babel"
            ],
            cacheDirectory: true,
            cacheCompression: isEnvProduction,
            compact: isEnvProduction
          }
        }
      },

      // --------------------------------------------------
      // Style [name].[ext]
      {
        test: cssRegex,
        exclude: cssModuleRegex,
        use: getStyleLoaders({})
      },
      {
        test: sassRegex,
        exclude: sassModuleRegex,
        use: getStyleLoaders({}, "sass-loader")
      },
      // TheEnd
      // --------------------------------------------------

      // --------------------------------------------------
      // Style Modules [name].module.[ext]
      {
        test: cssModuleRegex,
        use: getStyleLoaders({ modules: true, importLoaders: 1 })
      },
      {
        test: sassModuleRegex,
        use: getStyleLoaders({ modules: true, importLoaders: 1 }, "sass-loader")
      },
      // TheEnd
      // --------------------------------------------------

      {
        test: /\.(bmp|png|jpg|jpeg|gif|svg)$/,
        use: {
          loader: "file-loader",
          options: {
            name: "static/media/[name].[hash:4].[ext]"
          }
        }
      },
      {
        test: /\.(eot|ttf|woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
        use: {
          loader: "file-loader",
          options: {
            name: "static/fonts/[name].[ext]"
          }
        }
      }
    ]
  },

  plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      template: path.resolve(PATHS.public, "index.html"),
      filename: "./index.html"
    }),
    new CopyWebpackPlugin([{ from: "./public", to: "./" }], {
      copyUnmodified: true,
      ignore: ["node_modules", "./index.html"]
    }),
    new MiniCssExtractPlugin({
      filename: isEnvDevelopment
        ? "static/css/[name].css"
        : "static/css/[name].[contenthash:4].css",
      chunkFilename: isEnvDevelopment
        ? "static/css/[name].chunk.css"
        : "static/css/[name].[contenthash:4].chunk.css"
    }),
    new ManifestPlugin({
      fileName: "asset-manifest.json",
      publicPath: "/",
      generate: (seed, files) => {
        const manifestFiles = files.reduce(function(manifest, file) {
          manifest[file.name] = file.path;
          return manifest;
        }, seed);

        return {
          files: manifestFiles
        };
      }
    }),
    isEnvDevelopment && new webpack.HotModuleReplacementPlugin(),
    isEnvDevelopment && new webpack.NoEmitOnErrorsPlugin()
  ].filter(Boolean),

  node: {
    module: "empty",
    dgram: "empty",
    dns: "mock",
    fs: "empty",
    http2: "empty",
    net: "empty",
    tls: "empty",
    child_process: "empty"
  },
  stats: {
    cached: false,
    cachedAssets: false,
    chunks: false,
    chunkModules: false,
    colors: true,
    hash: false,
    modules: false,
    reasons: false,
    timings: true,
    version: false
  }
};
