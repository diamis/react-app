const path = require("path");

module.exports = {
  components: path.resolve(__dirname, "../../src/components"),
  helpers: path.resolve(__dirname, "../../src/helpers"),
  modules: path.resolve(__dirname, "../../src/modules"),
  services: path.resolve(__dirname, "../../src/services"),
  assets: path.resolve(__dirname, "../../src/assets"),
  pages: path.resolve(__dirname, "../../src/pages")
};
