import React, { PureComponent } from "react";
import { Switch, Route } from "react-router";

import routes from "helpers/router";
import NotFound from "pages/notFound";

class Root extends PureComponent {
  render() {
    return (
      <>
        <Switch>
          {routes.map(item => (
            <Route key={`route-${item.name}`} {...item} />
          ))}
          <Route component={NotFound} />
        </Switch>
      </>
    );
  }
}

export default Root;
