import React, { PureComponent } from "react";
import { Header, Footer } from "components/Layout";

import "assets/style/index.scss";
import "./Layout.scss";

class Layout extends PureComponent {
    constructor(props) {
        super(props);
        this.refMain = React.createRef();
        this.state = {};
    }

    componentDidMount() {
        this.onResize();
        if (window) window.addEventListener("resize", this.onResize);
    }

    onResize = () => {
        // if SSR exit;
        if (!window) return;

        let height = 0;
        const windowHeight = window.innerHeight;
        const header = document.body.querySelector("header.Header");
        const footer = document.body.querySelector("header.Header");
        const main = this.refMain.current;

        height += header ? header.clientHeight : 0;
        height += footer ? footer.clientHeight : 0;

        if (main.clientHeight + height < windowHeight) {
            main.style.minHeight = `${windowHeight - height}px`;
        }
    };

    static Content = ({ children }) => {
        return <div className="Main_content content">{children}</div>;
    };

    render() {
        const { children } = this.props;
        return (
            <>
                <Header />
                <main className="Main" ref={this.refMain}>
                    {children}
                </main>
                <Footer />
            </>
        );
    }

    componentWillUnmount() {
        if (window) window.removeEventListener("resize", this.onResize);
    }
}

export default Layout;
