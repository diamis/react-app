import React, { PureComponent } from "react";

import { Menu } from "components/Layout";
import { Logo } from "components/Logo";

import "./Header.scss";

class Header extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (
            <header className="Header">
                <div className="Header_content content">
                    <Logo />
                    <Menu />
                </div>
            </header>
        );
    }
}

export default Header;
