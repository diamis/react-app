import React, { PureComponent } from "react";

import "./Footer.scss";

class Footer extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (
            <footer className="Footer">
                <div className="Footer_content content">footer</div>
            </footer>
        );
    }
}

export default Footer;
