import React, { PureComponent } from "react";

import "./Menu.scss";

class Menu extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (
            <ul className="Menu">
                <li className="MenuItem">menu item 1</li>
                <li className="MenuItem">menu item 1</li>
                <li className="MenuItem">menu item 1</li>
            </ul>
        );
    }
}

export default Menu;
