import dotenv from "dotenv";
import React from "react";
import ReactDOM from "react-dom";
import Loadable from "react-loadable";
import { Provider } from "react-redux";
import { Router } from "react-router-dom";

import store from "helpers/createStore";
import history from "helpers/history";
import Root from "./Root";

dotenv.config();

window.onload = () => {
  Loadable.preloadReady().then(() => {
    const renderMethod = !!module.hot ? ReactDOM.render : ReactDOM.hydrate;
    renderMethod(
      <Provider store={store}>
        <Router history={history}>
          <Root />
        </Router>
      </Provider>,
      document.getElementById("root")
    );
  });
};

// if (process.env.NODE_ENV === "development") {
//   if (module.hot) {
//     module.hot.accept();
//   }
// }
