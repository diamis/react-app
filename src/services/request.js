import axios from "axios";
import { getToken } from "services/utils";

const ENV = process.env;
const API_HOST = ENV.REACT_APP_API_HOST;

const prepareUrl = url => {
  return `${API_HOST}/${url}`;
};

const authHeader = () => {
  const BearerToken = getToken();
  return BearerToken ? { Authorization: BearerToken } : {};
};

const request = (method, endPoint, { data, params, headers } = {}) => {
  const config = {
    url: prepareUrl(endPoint),
    method,
    params,
    data
  };

  const result = axios({
    ...config,
    headers: { ...authHeader(), ...(headers || {}) }
  }).catch(error => {
    return Promise.reject({
      success: false,
      statusCode: 600,
      message: error.message || "Network Error"
    });
  });

  return result;
};

export const get = (endPoint, data, options) => {
  return request("get", endPoint, data, options);
};

export const post = (endPoint, data, options) => {
  return request("post", endPoint, data, options);
};

export const put = (endPoint, data, options) => {
  return request("put", endPoint, data, options);
};

export const del = (endPoint, data, options) => {
  return request("delete", endPoint, data, options);
};
