import Cookies from "js-cookie";

export function setToken(token) {
  if (token) Cookies.set("token", token, { expires: 7 });
  return;
}

export const getToken = () => {
  let token = Cookies.get("token");
  return token;
};

export const clearToken = () => Cookies.remove("token");

export const parseUrl = search =>
  search
    .replace("?", "")
    .split("&")
    .reduce((result, item) => {
      const attr = item.split("=");
      if (attr && attr.length === 2) result[attr[0]] = attr[1];
      return result;
    }, {});

export const queryString = params =>
  Object.keys(params)
    .map(key => `${key}=${params[key]}`)
    .join("&");
