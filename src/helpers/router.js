import Home from "pages/home";

export default [
  {
    name: "home",
    path: "/",
    exact: true,
    component: Home
  }
];
