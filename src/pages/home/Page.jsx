import React from "react";
import { Layout } from "components/Layout";

const Page = () => (
    <Layout>
        <Layout.Content>
            <h1>Hello world!!</h1>
        </Layout.Content>
    </Layout>
);

export default Page;
