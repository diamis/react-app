import dotenv from "dotenv";
import express from "express";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";

import PATH from "../settings/path";
import { renderMiddleware, tokenMiddleware } from "../settings/middleware";

dotenv.config();
const app = express();
const PORT = process.env.PORT || 3000;

app.use(cookieParser());
app.use(bodyParser.json());
app.use(tokenMiddleware);
app.use("/static", express.static(PATH.static));
app.use("*", renderMiddleware);

app.listen(PORT, () => {
  console.log(`Server is listening http://localhost:${PORT}`);
});

export default app;
